const cp = require("child_process");
var fs = require("fs");
var stats = fs.statSync("../sample.txt")
var fileSizeInBytes = stats["size"];
var childProcess = 2;
var countProcess = 0;

if(fileSizeInBytes!=0)
{
    var child1 = cp.fork("./child1.js");
    var child2 = cp.fork("./child2.js");
    
    child1.on('error', (err) => {
        console.log("ERROR: fork failed! (" + err + ")");
    })
    .on("message", (data) => {
        countProcess++;   
    })
    .on("exit", ()=>{
        if(countProcess==childProcess)
        {
            console.log("file read operation is done and child2 run before child1");
            throw err;
        }
    });


    child2.on('error', (err) => {
        console.log("ERROR: fork failed! (" + err + ")");
    })
    .on("message", (data) => {
        countProcess++;    
    })
    .on("exit", ()=>{
        if(countProcess==childProcess)
        {
            console.log("file read operation is done and child1 run before child2");
        }
    });
}
else
{
    console.log("file is empty");
}
