var fs = require("fs");

var stats = fs.statSync("../sample.txt")
var fileSizeInBytes = stats["size"];

var buffer = new Buffer(fileSizeInBytes/2);

fs.open('../sample.txt', 'r+', function(err, fd) {
   if (err) {
      return console.error(err);
   }
 
   fs.readSync(fd, buffer, 0, buffer.length, 0);

   process.send(`${buffer.toString()}`);
   
   fs.close(fd, function(err) {
    if (err) {
       console.log(err);
    } 

    });
   
});

