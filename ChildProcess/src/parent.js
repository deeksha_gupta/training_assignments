//import * as cp from "child_process"
const cp = require("child_process");
const fs = require("fs");
const names = ["duly", "mack", "bill", "steve"];
var buffer = new Buffer('../sample.txt');
console.log(buffer.length);
var child = cp.fork("./dist/child1.js", names);
 
child.on("message", (data) => {
    console.log(`parent recevies ${data}`)
})
.on("exit", ()=>{
    console.log("child terminated");
});