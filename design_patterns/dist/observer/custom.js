"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
exports.customEE = new events_1.EventEmitter();
let count = 0;
setInterval(() => {
    exports.customEE.emit('ping', count);
    count++;
}, 3000);
//# sourceMappingURL=custom.js.map