"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Engineer {
    constructor(fName, lName) {
        this.firstName = fName;
        this.lastName = lName;
    }
    getName() {
        return ' Engineer ' + this.firstName + " " + this.lastName;
    }
}
exports.Engineer = Engineer;
//# sourceMappingURL=engineer.js.map