"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const empfactory_1 = require("./empfactory");
var EmpFactory = new empfactory_1.empFactory();
let engineer = EmpFactory.empFac('Roy', 'Agasthyan', 'engineer');
let manager = EmpFactory.empFac('Sam', 'Johnson', 'manager');
let director = EmpFactory.empFac('Jack', 'Daniel', 'director');
console.log(engineer.getName());
console.log(manager.getName());
console.log(director.getName());
//# sourceMappingURL=employee.js.map