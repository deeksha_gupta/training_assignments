"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Manager {
    constructor(fName, lName) {
        this.firstName = fName;
        this.lastName = lName;
    }
    getName() {
        return ' Manager ' + this.firstName + " " + this.lastName;
    }
}
exports.Manager = Manager;
//# sourceMappingURL=manager.js.map