"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const engineer_1 = require("./engineer");
const manager_1 = require("./manager");
const director_1 = require("./director");
class empFactory {
    empFac(fname, lname, type) {
        if (type === 'engineer') {
            return new engineer_1.Engineer(fname, lname);
        }
        else if (type === 'manager') {
            return new manager_1.Manager(fname, lname);
        }
        else if (type === 'director') {
            return new director_1.Director(fname, lname);
        }
    }
}
exports.empFactory = empFactory;
//# sourceMappingURL=empfactory.js.map