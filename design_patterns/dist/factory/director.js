"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Director {
    constructor(fName, lName) {
        this.firstName = fName;
        this.lastName = lName;
    }
    getName() {
        return ' Director ' + this.firstName + " " + this.lastName;
    }
}
exports.Director = Director;
//# sourceMappingURL=director.js.map