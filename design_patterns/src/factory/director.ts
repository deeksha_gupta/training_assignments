export class Director{
    firstName:string;
    lastName:string;
    constructor(fName:string, lName:string){
        this.firstName = fName
        this.lastName = lName
    }

    getName(){
        return ' Director ' + this.firstName + " " + this.lastName
    }
}