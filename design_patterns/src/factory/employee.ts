import {empFactory} from './empfactory'

var EmpFactory = new empFactory();
let engineer = EmpFactory.empFac('Roy','Agasthyan','engineer')
let manager = EmpFactory.empFac('Sam','Johnson','manager')
let director =EmpFactory.empFac('Jack', 'Daniel','director')

console.log(engineer.getName())
console.log(manager.getName())
console.log(director.getName())