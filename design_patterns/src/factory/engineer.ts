export class Engineer{
    firstName:string;
    lastName:string;
    constructor(fName:string, lName:string){
        this.firstName = fName
        this.lastName = lName
    }

    getName(){
        return ' Engineer ' + this.firstName + " " + this.lastName
    }
}