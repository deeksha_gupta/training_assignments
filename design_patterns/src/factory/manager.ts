export class Manager{
    firstName:string;
    lastName:string;
    constructor(fName:string, lName:string){
        this.firstName = fName
        this.lastName = lName
    }

    getName(){
        return ' Manager ' + this.firstName + " " + this.lastName
    }
}