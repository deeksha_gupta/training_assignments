import { customEE } from './custom';

customEE.once('ping', () => console.log('started pinging'))
  .on('ping', num => console.log(`ping #${num} from module`));