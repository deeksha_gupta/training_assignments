import * as mongo from "mongodb";

export class dbConnection {
    private static instance: dbConnection;

    private constructor() { }
    dbo: any = null;

    async DbConnect() {
        var clientObj:any; 
        var dbb:any;
        try {           
            let url = 'mongodb://localhost:27017';
            clientObj = await mongo.MongoClient.connect(url);
            dbb = clientObj.db('todo');
            return dbb;
        } catch (e) {
            return e;
        }
    }
    async getConnection() {
        try {
            if (!this.dbo) {

                this.dbo = await this.DbConnect();
                return this.dbo;
            } else {
                return this.dbo;
            }
        } catch (e) {
            return e;
        }
    }
    public static getInstance(): dbConnection {
        if (!dbConnection.instance) {
            dbConnection.instance = new dbConnection();
        }
        return dbConnection.instance;
    }
}

