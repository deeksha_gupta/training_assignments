import * as fs from 'fs';
import {Request, Response} from 'express';
import * as type from '../src/types'
import {CURD} from './CurdOperation'


export class fileRepositry implements CURD{

      list:type.todo_obj = {
          table:[]
      }; 
    

      insert_todo(data:type.todo_input, id:string):string
      {
            try
            { 
                  if(id=='' || data==null)
                  {
                        return("no id/data given");
                  }
                  else
                  {
                        var file_data = fs.readFileSync('./todo_list.json', 'utf8'); 
                        this.list = JSON.parse(file_data);
                        this.list.table.push(data);
                        fs.writeFileSync('./todo_list.json', JSON.stringify(this.list, null, 1));
                        return("user added successfully");
                  }
            }catch(e)
            {
            return("file not read/write properly");
            }
      }
      insert = (req: Request, res: Response):void =>{
        
            var data:type.todo_input;  
            data = req.body;
            var id:string = req.body._id;
      
            var output:string = this.insert_todo(data, id);
            res.send(output);
      
      }
      delete_todo(id:string):string
      {
            try{
                  if(id=='')
                  {
                        return("no id is given");
                  }
                  else
                  {
                        var file_data, id:string, index:number, data1, p:number =0;
                        var val1:type.todo_obj = {
                              table:[]
                        };
                        file_data = fs.readFileSync('./todo_list.json', 'utf8');
                                    
                        val1 = JSON.parse(file_data);
                        index = val1.table.findIndex(function(item, i){
                              return item._id === id;
                        });
                        
                        delete val1.table[index];
                        
                        this.list.table = [];
                        for(var i=0;i<val1.table.length;i++)
                        {
                              if(val1.table[i] != null)
                              {
                              this.list.table[p] = val1.table[i];
                              p++;
                              }
                        }

                        data1 = JSON.stringify(this.list, null, 1);  
                  
                        fs.writeFileSync('./todo_list.json', data1);
                        return("user deleted successfully");
                  }
                  
            }catch(e)
            {
                  return("problem in reading/writting file");
            }
      }
      delete = (req: Request, res: Response):void =>{
          
            var id = req.body._id;
            var output:string = this.delete_todo(id);
            
            res.send(output);
         
      }
      edit_todo(data:type.todo_input,id:string):string
      {
            try{
                  if(id=='' || data==null)
                  {
                        return('id/data is not given');
                  }
                  else
                  {
                        var file_data, id:string, data_string, index; 
                        file_data = fs.readFileSync('./todo_list.json', 'utf8');
      
                        this.list = JSON.parse(file_data);
                        index = this.list.table.findIndex(function(item, i){
                        return item._id === id;
                        });
      
                        this.list.table[index] = data;
                        data_string = JSON.stringify(this.list, null, 1);       
                                    
                        fs.writeFileSync('./todo_list.json', data_string);
                        return('user edited succssfully');
                  }                 
            }catch(e)
            {
                   return("problem in reading/writting file");
            } 
      }
      edit = (req: Request, res: Response):void =>
      {
            var id:string = req.body._id; 
            var output:string = this.edit_todo(req.body,id);
      
            res.send(output);
           
      }
      show_todo():string
      {
            var file_data;
            try{
                  file_data = fs.readFileSync('./todo_list.json', 'utf8');

                  this.list = JSON.parse(file_data);
                  return("showing the data");
              }catch(e){
                  return("Their is some problem in reading or writing the file");
            }        

      }
      show = (req: Request, res: Response):void =>
      {
            var output:(string|Array<type.todo_input>) = this.show_todo();
            if(output=="showing the data")
            {
                  res.send(this.list.table);
            }
            else
            {
                  res.send(output);
            }
      }
}