import {Request, Response} from 'express';
import * as type from '../src/types'
import {dbConnection} from './dbConnection';
import {CURD} from './CurdOperation';


let instance = dbConnection.getInstance();

export class dbRepositry implements CURD{
    
    async insert_todo(data:type.todo_input,id:string):Promise<string>
    {
        try{ 
            if(id=='' || data==null)
            {
                return("no id/data given");
            }
            else
            {
                let db = await instance.getConnection();
                await db.collection("todo_list").insertOne(data);
                return("user added successfully");
                //return obj;
            }
        }catch(e)
        {
            return("db not build properly");
        }
    }

    insert = (req: Request, res: Response) =>
    { 
        var data:type.todo_input;       
        data = req.body;
        var id:string = req.body._id;
        this.insert_todo(data,id).then(x =>{
            res.send(x);
        })
                
    }

    async delete_todo(id:string):Promise<string>
    {
        try{
            if(id=='')
            {
                return("id is not given");
            }
            else
            {
                let db = await instance.getConnection();
                await db.collection("todo_list").deleteOne(id);
                return("user deleted succssfully");
            }
           

        }catch(e)
        {
            return("db not build properly");
        }
    }
    delete = (req: Request, res: Response) =>
    {
        var id;
        id = req.body._id;
        this.delete_todo(id).then(x =>{
            res.send(x);
        })
    }

    async edit_todo(data:any,id:any):Promise<string>
    {
        try{
            if(id._id=='' || data==null)
            {
                return("id/data not given");
            }
            else
            {
                let db = await instance.getConnection();
                await db.collection("todo_list").updateOne(id, data);
                return("user edited succssfully")
            }  
        }catch(e)
        {
            return("db not read properly");
        }
    }
    edit = (req: Request, res: Response) =>
    {
        var myquery = { _id: req.body._id };
        var newvalues = { $set: req.body };
        this.edit_todo(newvalues, myquery).then(x =>{
        res.send(x);
        })
    }

    async show_todo():Promise<string>
    {
        var data:any;          
        try{
            let db = await instance.getConnection();
            data = await db.collection("todo_list").find({}).toArray();
            return data;
        }catch(e){
            return("Their is some problem in reading or writing the file");
        }     
    }
    show  = (req: Request, res: Response) =>
    {
        this.show_todo().then(x =>{
        res.send(x);
        })
    }
}