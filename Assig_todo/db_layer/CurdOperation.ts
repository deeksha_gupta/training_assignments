import {Request, Response} from 'express';

export interface CURD
{
     insert:(req: Request, res: Response) => void;
     delete:(req: Request, res: Response) => void;
     edit:(req: Request, res: Response) => void;
     show:(req: Request, res: Response) => void;
}