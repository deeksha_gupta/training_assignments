import {dbRepositry} from './dbRepositry'
import {fileRepositry} from './fileRepositry'



export class registrationRepositry
{
    db_Operation: dbRepositry = null;
    file_Operation: fileRepositry = null;
    constructor(){
        this.db_Operation = new dbRepositry();
        this.file_Operation = new fileRepositry();
    }
    public getRepositry(StoreDataInDb:boolean): (dbRepositry|fileRepositry)
    {
        return StoreDataInDb ? this.db_Operation: this.file_Operation;
    }
}
