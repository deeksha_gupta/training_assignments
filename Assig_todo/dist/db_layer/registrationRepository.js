"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dbRepositry_1 = require("./dbRepositry");
const fileRepositry_1 = require("./fileRepositry");
class registrationRepositry {
    constructor() {
        this.db_Operation = null;
        this.file_Operation = null;
        this.db_Operation = new dbRepositry_1.dbRepositry();
        this.file_Operation = new fileRepositry_1.fileRepositry();
    }
    getRepositry(StoreDataInDb) {
        return StoreDataInDb ? this.db_Operation : this.file_Operation;
    }
}
exports.registrationRepositry = registrationRepositry;
//# sourceMappingURL=registrationRepository.js.map