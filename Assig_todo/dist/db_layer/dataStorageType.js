"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
const db_repositry_1 = require("./db_repositry");
const file_repositry_1 = require("./file_repositry");
let file_Operation = new file_repositry_1.file_repositry();
let db_Operation = new db_repositry_1.db_repositry();
class storage_choice {
    constructor() {
        this.add = (req, res) => {
            var output;
            if (config_1.StoreDataInDb == true) {
                output = db_Operation.insert_todo(req, res);
            }
            else {
                output = file_Operation.insert_todo(req, res);
            }
        };
        this.delete = (req, res) => {
            var output;
            if (config_1.StoreDataInDb == true) {
                output = db_Operation.delete_todo(req, res);
            }
            else {
                output = file_Operation.delete_todo(req, res);
            }
        };
        this.edit = (req, res) => {
            var output;
            if (config_1.StoreDataInDb == true) {
                output = db_Operation.edit_todo(req, res);
            }
            else {
                output = file_Operation.edit_todo(req, res);
            }
        };
        this.show = (req, res) => {
            var output;
            if (config_1.StoreDataInDb == true) {
                output = db_Operation.show_todo(req, res);
            }
            else {
                output = file_Operation.show_todo(req, res);
            }
        };
    }
}
exports.storage_choice = storage_choice;
//# sourceMappingURL=dataStorageType.js.map