"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo = require("mongodb");
class DbClient {
    connect() {
        var url = "mongodb://localhost:27017/";
        mongo.MongoClient.connect(url, function (err, db) {
            if (err)
                throw err;
            var dbo = db.db("mydb1");
            var myobj = { name: "Company Inc", address: "Highway 37" };
            dbo.collection("customer").insertOne(myobj, function (err, res) {
                if (err)
                    throw err;
                console.log("1 document inserted");
                db.close();
            });
        });
    }
}
let dbCon = new DbClient();
dbCon.connect();
//# sourceMappingURL=db_choise.js.map