"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_repositry_1 = require("./db_repositry");
const file_repositry_1 = require("./file_repositry");
let db_Operation = new db_repositry_1.db_repositry();
let file_Operation = new file_repositry_1.file_repositry();
class ConnectionFactory {
    getInstance(StoreDataInDb) {
        if (StoreDataInDb == true) {
            return (db_Operation);
        }
        else {
            return (file_Operation);
        }
    }
}
exports.ConnectionFactory = ConnectionFactory;
/*let file_Operation = new file_repositry();
let db_Operation = new db_repositry();

export class storage_choice
{
    add = (req: Request, res: Response):void =>{
          var output:string;
          if(StoreDataInDb == true){
              output = db_Operation.insert_todo(req, res);
          }else{
             output = file_Operation.insert_todo(req, res);
          }
    }

    delete = (req: Request, res: Response):void =>{
      var output:string;
          if(StoreDataInDb == true){
              output = db_Operation.delete_todo(req, res);
          }else{
              output = file_Operation.delete_todo(req, res);
          }
    }

    edit = (req: Request, res: Response):void =>{
      var output:string;
          if(StoreDataInDb == true){
              output = db_Operation.edit_todo(req, res);
          }else{
              output = file_Operation.edit_todo(req, res);
          }
    }

    show = (req: Request, res: Response):void =>{
      var output:string;
          if(StoreDataInDb == true){
              output = db_Operation.show_todo(req, res);
          }else{
              output = file_Operation.show_todo(req, res);
          }
    }
}*/
//# sourceMappingURL=ConnectionFactory.js.map