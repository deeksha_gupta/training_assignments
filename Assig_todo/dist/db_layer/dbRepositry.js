"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dbConnection_1 = require("./dbConnection");
let instance = dbConnection_1.dbConnection.getInstance();
class dbRepositry {
    constructor() {
        this.insert = (req, res) => {
            var data;
            data = req.body;
            var id = req.body._id;
            this.insert_todo(data, id).then(x => {
                res.send(x);
            });
        };
        this.delete = (req, res) => {
            var id;
            id = req.body._id;
            this.delete_todo(id).then(x => {
                res.send(x);
            });
        };
        this.edit = (req, res) => {
            var myquery = { _id: req.body._id };
            var newvalues = { $set: req.body };
            this.edit_todo(newvalues, myquery).then(x => {
                res.send(x);
            });
        };
        this.show = (req, res) => {
            this.show_todo().then(x => {
                res.send(x);
            });
        };
    }
    insert_todo(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (id == '' || data == null) {
                    return ("no id/data given");
                }
                else {
                    let db = yield instance.getConnection();
                    yield db.collection("todo_list").insertOne(data);
                    return ("user added successfully");
                    //return obj;
                }
            }
            catch (e) {
                return ("db not build properly");
            }
        });
    }
    delete_todo(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (id == '') {
                    return ("id is not given");
                }
                else {
                    let db = yield instance.getConnection();
                    yield db.collection("todo_list").deleteOne(id);
                    return ("user deleted succssfully");
                }
            }
            catch (e) {
                return ("db not build properly");
            }
        });
    }
    edit_todo(data, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (id._id == '' || data == null) {
                    return ("id/data not given");
                }
                else {
                    let db = yield instance.getConnection();
                    yield db.collection("todo_list").updateOne(id, data);
                    return ("user edited succssfully");
                }
            }
            catch (e) {
                return ("db not read properly");
            }
        });
    }
    show_todo() {
        return __awaiter(this, void 0, void 0, function* () {
            var data;
            try {
                let db = yield instance.getConnection();
                data = yield db.collection("todo_list").find({}).toArray();
                return data;
            }
            catch (e) {
                return ("Their is some problem in reading or writing the file");
            }
        });
    }
}
exports.dbRepositry = dbRepositry;
//# sourceMappingURL=dbRepositry.js.map