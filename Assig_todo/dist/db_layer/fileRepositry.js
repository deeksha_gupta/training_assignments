"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs"));
class fileRepositry {
    constructor() {
        this.list = {
            table: []
        };
        this.insert = (req, res) => {
            var data;
            data = req.body;
            var id = req.body._id;
            var output = this.insert_todo(data, id);
            res.send(output);
        };
        this.delete = (req, res) => {
            var id = req.body._id;
            var output = this.delete_todo(id);
            res.send(output);
        };
        this.edit = (req, res) => {
            var id = req.body._id;
            var output = this.edit_todo(req.body, id);
            res.send(output);
        };
        this.show = (req, res) => {
            var output = this.show_todo();
            if (output == "showing the data") {
                res.send(this.list.table);
            }
            else {
                res.send(output);
            }
        };
    }
    insert_todo(data, id) {
        try {
            if (id == '' || data == null) {
                return ("no id/data given");
            }
            else {
                var file_data = fs.readFileSync('./todo_list.json', 'utf8');
                this.list = JSON.parse(file_data);
                this.list.table.push(data);
                fs.writeFileSync('./todo_list.json', JSON.stringify(this.list, null, 1));
                return ("user added successfully");
            }
        }
        catch (e) {
            return ("file not read/write properly");
        }
    }
    delete_todo(id) {
        try {
            if (id == '') {
                return ("no id is given");
            }
            else {
                var file_data, id, index, data1, p = 0;
                var val1 = {
                    table: []
                };
                file_data = fs.readFileSync('./todo_list.json', 'utf8');
                val1 = JSON.parse(file_data);
                index = val1.table.findIndex(function (item, i) {
                    return item._id === id;
                });
                delete val1.table[index];
                this.list.table = [];
                for (var i = 0; i < val1.table.length; i++) {
                    if (val1.table[i] != null) {
                        this.list.table[p] = val1.table[i];
                        p++;
                    }
                }
                data1 = JSON.stringify(this.list, null, 1);
                fs.writeFileSync('./todo_list.json', data1);
                return ("user deleted successfully");
            }
        }
        catch (e) {
            return ("problem in reading/writting file");
        }
    }
    edit_todo(data, id) {
        try {
            if (id == '' || data == null) {
                return ('id/data is not given');
            }
            else {
                var file_data, id, data_string, index;
                file_data = fs.readFileSync('./todo_list.json', 'utf8');
                this.list = JSON.parse(file_data);
                index = this.list.table.findIndex(function (item, i) {
                    return item._id === id;
                });
                this.list.table[index] = data;
                data_string = JSON.stringify(this.list, null, 1);
                fs.writeFileSync('./todo_list.json', data_string);
                return ('user edited succssfully');
            }
        }
        catch (e) {
            return ("problem in reading/writting file");
        }
    }
    show_todo() {
        var file_data;
        try {
            file_data = fs.readFileSync('./todo_list.json', 'utf8');
            this.list = JSON.parse(file_data);
            return ("showing the data");
        }
        catch (e) {
            return ("Their is some problem in reading or writing the file");
        }
    }
}
exports.fileRepositry = fileRepositry;
//# sourceMappingURL=fileRepositry.js.map