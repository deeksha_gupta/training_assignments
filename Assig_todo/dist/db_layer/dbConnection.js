"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongo = __importStar(require("mongodb"));
class dbConnection {
    constructor() {
        this.dbo = null;
    }
    DbConnect() {
        return __awaiter(this, void 0, void 0, function* () {
            var clientObj;
            var dbb;
            try {
                let url = 'mongodb://localhost:27017';
                clientObj = yield mongo.MongoClient.connect(url);
                dbb = clientObj.db('todo');
                return dbb;
            }
            catch (e) {
                return e;
            }
        });
    }
    getConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!this.dbo) {
                    this.dbo = yield this.DbConnect();
                    return this.dbo;
                }
                else {
                    return this.dbo;
                }
            }
            catch (e) {
                return e;
            }
        });
    }
    static getInstance() {
        if (!dbConnection.instance) {
            dbConnection.instance = new dbConnection();
        }
        return dbConnection.instance;
    }
}
exports.dbConnection = dbConnection;
//# sourceMappingURL=dbConnection.js.map