"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const dbConnection_1 = require("./dbConnection");
//let db_obj = new Db_connect();
let instance = dbConnection_1.dbConnection.getInstance();
class db_repositry {
    constructor() {
        this.insert = (req, res) => __awaiter(this, void 0, void 0, function* () {
            var data;
            //db_obj.connect(function(client: any){
            /* db_obj.connect(function(client: any){
             data = req.body;
             client.collection("todo_list").insertOne(data, function(err: any, obj: any) {
                 if (err) throw err;
                 console.log("1 document inserted");
             });
         });   */
            let db = yield instance.getConnection();
            //console.log("got a db");
            data = req.body;
            //console.log("data got");
            //console.log(db);
            var obj = yield db.collection("todo_list").insertOne(data);
            res.send("user added succssfully");
        });
        this.delete = (req, res) => __awaiter(this, void 0, void 0, function* () {
            var data;
            /*db_obj.connect(function(client: any){
                data = req.body;
                client.collection("todo_list").deleteOne(data, function(err:any, res:any) {
                    if (err) throw err;
                    console.log("1 document deleted");
                });
            });*/
            let db = yield instance.getConnection();
            data = req.body;
            yield db.collection("todo_list").deleteOne(data);
            res.send("user deleted succssfully");
        });
        this.edit = (req, res) => __awaiter(this, void 0, void 0, function* () {
            /*db_obj.connect(function(client: any){
                var myquery = { _id: req.body._id };
                var newvalues = { $set: req.body };
                client.collection("todo_list").updateOne(myquery, newvalues, function(err:any, res:any) {
                    if (err) throw err;
                    console.log("1 document updated");
                });
            });   */
            let db = yield instance.getConnection();
            var myquery = { _id: req.body._id };
            var newvalues = { $set: req.body };
            yield db.collection("todo_list").updateOne(myquery, newvalues);
            res.send("user edited succssfully");
        });
        this.show = (req, res) => __awaiter(this, void 0, void 0, function* () {
            var data;
            /*db_obj.connect(function(client: any){
                var data1;
                client.collection("todo_list").find({}).toArray(function(err:any, result:any) {
                    if (err) throw err;
                    data = result;
                    console.log(result);
                    //To_show_result(result);
                    res.send(result);
                });
            });   */
            let db = yield instance.getConnection();
            data = yield db.collection("todo_list").find({}).toArray();
            res.send(data);
        });
    }
}
exports.db_repositry = db_repositry;
//# sourceMappingURL=db_repositry.js.map