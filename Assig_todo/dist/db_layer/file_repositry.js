"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
class file_repositry {
    constructor() {
        this.list = {
            table: []
        };
        this.insert = (req, res) => {
            var file_data;
            file_data = fs.readFileSync('./todo_list.json', 'utf8');
            this.list = JSON.parse(file_data);
            this.list.table.push(req.body);
            fs.writeFileSync('./todo_list.json', JSON.stringify(this.list, null, 1));
            res.send("user added succssfully");
        };
        this.delete = (req, res) => {
            var file_data, id, index, data1, p = 0;
            var val1 = {
                table: []
            };
            file_data = fs.readFileSync('./todo_list.json', 'utf8');
            val1 = JSON.parse(file_data);
            id = req.body._id;
            index = val1.table.findIndex(function (item, i) {
                return item._id === id;
            }); //remove findIndex method use set here.
            delete val1.table[index];
            this.list.table = [];
            for (var i = 0; i < val1.table.length; i++) //remove 
             {
                if (val1.table[i] != null) {
                    this.list.table[p] = val1.table[i];
                    p++;
                }
            }
            data1 = JSON.stringify(this.list, null, 1);
            fs.writeFileSync('./todo_list.json', data1);
            res.send("user deleted succssfully");
        };
        this.edit = (req, res) => {
            var file_data, id, data_string, index;
            file_data = fs.readFileSync('./todo_list.json', 'utf8');
            this.list = JSON.parse(file_data);
            id = req.body._id;
            index = this.list.table.findIndex(function (item, i) {
                return item._id === id;
            }); //same
            this.list.table[index] = req.body;
            data_string = JSON.stringify(this.list, null, 1);
            fs.writeFileSync('./todo_list.json', data_string);
            res.send("user edited succssfully");
        };
        this.show = (req, res) => {
            var file_data, show, count = 1, str = '';
            file_data = fs.readFileSync('./todo_list.json', 'utf8');
            this.list = JSON.parse(file_data);
            res.send(this.list.table);
        };
    }
}
exports.file_repositry = file_repositry;
//# sourceMappingURL=file_repositry.js.map