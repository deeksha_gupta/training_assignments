"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongo = require("mongodb");
class Db_connect {
    connect(cb) {
        var url = "mongodb://localhost:27017/";
        var dbo;
        if (dbo) {
            return dbo;
        }
        else {
            mongo.MongoClient.connect(url, function (err, db) {
                if (err)
                    throw err;
                dbo = db.db("todo");
                cb(dbo);
            });
        }
    }
}
exports.Db_connect = Db_connect;
//# sourceMappingURL=db_connect.js.map