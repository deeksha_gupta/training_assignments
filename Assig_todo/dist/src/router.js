"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const registrationRepository_1 = require("../db_layer/registrationRepository");
const config_1 = require("../config");
const application_1 = require("./application");
class router {
    constructor() {
        application_1.app.set("port", process.env.PORT || 4100);
    }
    loadRoutes() {
        let dbOperation;
        dbOperation = new registrationRepository_1.registrationRepositry().getRepositry(config_1.StoreDataInDb);
        /*app.get('/', function (req:Request, res:Response) {
                res.sendFile( __dirname + "/" + "front_page.html" );
        })*/
        application_1.app.post('/todo', dbOperation.insert);
        application_1.app.delete('/todo', dbOperation.delete);
        application_1.app.put('/todo', dbOperation.edit);
        application_1.app.get('/todo', dbOperation.show);
    }
}
exports.router = router;
//# sourceMappingURL=router.js.map