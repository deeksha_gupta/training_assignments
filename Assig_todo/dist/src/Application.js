"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const router_1 = require("./router");
exports.app = express();
exports.app.use(bodyParser.urlencoded({ extended: false }));
let Route = new router_1.router();
const server = exports.app.listen(exports.app.get("port"), () => {
    console.log("App is running on http://localhost:%d in %s mode", exports.app.get("port"), exports.app.get("env"));
    Route.loadRoutes();
});
//# sourceMappingURL=application.js.map