import {registrationRepositry} from '../db_layer/registrationRepository'
import{StoreDataInDb} from '../config'
import {app} from './application';
import { dbRepositry } from '../db_layer/dbRepositry';
import { fileRepositry } from '../db_layer/fileRepositry';


export class router
{
    constructor(){
        app.set("port", process.env.PORT || 4100);
    }
    loadRoutes(){

        let dbOperation: (dbRepositry|fileRepositry);
        dbOperation = new registrationRepositry().getRepositry(StoreDataInDb)
    
        /*app.get('/', function (req:Request, res:Response) {
                res.sendFile( __dirname + "/" + "front_page.html" );  
        })*/
        app.post('/todo', dbOperation.insert);

        app.delete('/todo', dbOperation.delete);

        app.put('/todo', dbOperation.edit);

        app.get('/todo', dbOperation.show);

    }
}
