import express = require("express");
import bodyParser = require('body-parser');
import {router} from './router'

export const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

let Route = new router();

const server = app.listen(app.get("port"), () => {
    console.log(
      "App is running on http://localhost:%d in %s mode",
      app.get("port"),
      app.get("env") 
    );
    Route.loadRoutes();
});