import * as chai from "chai";
import * as fs from 'fs';
import * as type from '../src/types'
var expect = chai.expect;
import {fileRepositry} from '../db_layer/fileRepositry';

describe('todo insert in file', function() {

    it('insert_todo() should not add to json file if no id is passed in', function() {
      var file_repositry = new fileRepositry();
      var input1 = {
        title: "TODO242",
        _id:"",
        discription: "After need to do",
        todayDate: "12/1/17",
        targetDate: "12/11/20"
       }

      expect(file_repositry.insert_todo(input1,'')).to.equal("no id/data given");
    });
     
    it('insert_todo() should not add to json file if data is not passed in', function() {
        var file_repositry = new fileRepositry();
        var input1 = null;
        expect(file_repositry.insert_todo(input1,'242')).to.equal("no id/data given");
    });

    it('insert_todo() should not add to json file if data and id is not passed in', function() {
      var file_repositry = new fileRepositry();
      var input1 = null;
      expect(file_repositry.insert_todo(input1,'')).to.equal("no id/data given");
    });

    it('insert_todo() should add to json file if id and data is passed in', function() {
        var file_repositry = new fileRepositry();
        var input1 = {
          title: "TODO242",
          _id:"242",
          discription: "After need to do",
          todayDate: "12/1/17",
          targetDate: "12/11/20"
         }
        
            expect(file_repositry.insert_todo(input1,'242')).to.equal("user added successfully");

    });
    
});

describe('todo delete in file', function() {

  it('delete_todo() should not delete from json file if no id is passed in', function() {
    var file_repositry = new fileRepositry();

    expect(file_repositry.delete_todo('')).to.equal("no id is given");
  });

  it('delete_todo() should delete from json file if id is passed in', function() {
      var file_repositry = new fileRepositry();
      
      expect(file_repositry.delete_todo('242')).to.equal("user deleted successfully");

  });
  
});

describe('todo edit in file', function() {

  it('edit_todo() should not edit from json file if no id is passed in', function() {
    var file_repositry = new fileRepositry();
    var input1 = {
      title: "TODO242",
      _id:"",
      discription: "After need to do",
      todayDate: "12/1/17",
      targetDate: "12/11/20"
     }

    expect(file_repositry.edit_todo(input1,'')).to.equal("id/data is not given");
  });
   
  it('edit_todo() should not edit from json file if data is not passed in', function() {
      var file_repositry = new fileRepositry();
      var input1 = null;
      expect(file_repositry.edit_todo(input1,'242')).to.equal("id/data is not given");
  });

  it('edit_todo() should not edit from json file if data and id is not passed in', function() {
    var file_repositry = new fileRepositry();
    var input1 = null;
    expect(file_repositry.edit_todo(input1,'')).to.equal("id/data is not given");
  });

  it('edit_todo() should edit from json file if id and data is passed in', function() {
      var file_repositry = new fileRepositry();
      var input1 = {
        title: "TODO242",
        _id:"242",
        discription: "After need to do",
        todayDate: "12/1/17",
        targetDate: "12/11/20"
      }
      
      expect(file_repositry.edit_todo(input1,'242')).to.equal("user edited succssfully");

  });
  
});

describe('todo show in file', function() {

  it('show_todo() should show json file if file reading is proper', function() {
      var file_repositry = new fileRepositry();
      var file_data = fs.readFileSync('./todo_list.json', 'utf8');
      var list:type.todo_obj = {
        table:[]
      }; 
      list = JSON.parse(file_data);
      expect(file_repositry.show_todo()).to.equal("showing the data");
  });
  
});