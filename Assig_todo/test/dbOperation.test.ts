import * as chai from "chai";
import * as sinon from "sinon"
var expect = chai.expect;
import {dbRepositry} from '../db_layer/dbRepositry';
import {dbConnection} from '../db_layer/dbConnection';

let instance = dbConnection.getInstance();

describe('insert todo in database', function () {

    it('insert_todo() should not add to database if no id is passed in', async ()=> {
       var input1 = {
            title: "TODO242",
            _id:"242",
            discription: "After need to do",
            todayDate: "12/1/17",
            targetDate: "12/11/20"
           }
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'insert_todo');
       
        stub.withArgs(input1,'').callsFake(async function foo(){
            return "no id/data given";
        })

        let output = await db_repositry.insert_todo(input1,'');
        expect(output).to.equal("no id/data given");

    });

    it('insert_todo() should not add to database if data is not passed in', async ()=> {
        var input1 = null;
         let db_repositry = new dbRepositry();
         
         var stub = sinon.stub(db_repositry,'insert_todo');
        
         stub.withArgs(input1,'242').callsFake(async function foo(){
             return "no id/data given";
         })
 
         let output = await db_repositry.insert_todo(input1,'242');
         expect(output).to.equal("no id/data given");
 
     });

    it('insert_todo() should not add to database if data and id is not passed in', async ()=> {
    var input1 = null;
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'insert_todo');
    
        stub.withArgs(input1,'').callsFake(async function foo(){
            return "no id/data given";
        })

        let output = await db_repositry.insert_todo(input1,'');
        expect(output).to.equal("no id/data given");

    });

    it('insert_todo() should add to database if id is passed in', async ()=> {
    var input1 = {
            title: "TODO242",
            _id:"242",
            discription: "After need to do",
            todayDate: "12/1/17",
            targetDate: "12/11/20"
        }
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'insert_todo');
    
        stub.withArgs(input1,'242').callsFake(async function foo(){
            return "user added successfully";
        })

        let output = await db_repositry.insert_todo(input1,'242');
        expect(output).to.equal("user added successfully");

    });
   
});

describe('todo delete in database', function() {

    it('delete_todo() should not delete from database if no id is passed in', async function() {
       
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'delete_todo');
       
        stub.withArgs('').callsFake(async function foo(){
            return "id is not given";
        })

        let output = await db_repositry.delete_todo('');
        expect(output).to.equal("id is not given");
    });
  
    it('delete_todo() should delete from database if id is passed in', async function() {
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'delete_todo');
       
        stub.withArgs('24').callsFake(async function foo(){
            return "id is not given";
        })

        let output = await db_repositry.delete_todo('24');
        expect(output).to.equal("id is not given");
  
    });
    
  });

describe('todo edit in database', function() {

    it('edit_todo() should not edit to database if no id is passed in', async ()=> {
        var input1 = {
             title: "TODO242",
             _id:"242",
             discription: "After need to do",
             todayDate: "12/1/17",
             targetDate: "12/11/20"
            }
         let db_repositry = new dbRepositry();
         
         var stub = sinon.stub(db_repositry,'edit_todo');
        
         stub.withArgs(input1,'').callsFake(async function foo(){
             return "id/data not given";
         })
 
         let output = await db_repositry.edit_todo(input1,'');
         expect(output).to.equal("id/data not given");
 
     });
     
    it('edit_todo() should not edit to database if data is not passed in', async ()=> {
    var input1 = null;
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'edit_todo');
    
        stub.withArgs(input1,'242').callsFake(async function foo(){
            return "id/data not given";
        })

        let output = await db_repositry.edit_todo(input1,'242');
        expect(output).to.equal("id/data not given");

    });
  
    it('edit_todo() should not edit to database if data and id is not passed in', async ()=> {
        var input1 = null;
            let db_repositry = new dbRepositry();
            
            var stub = sinon.stub(db_repositry,'edit_todo');
        
            stub.withArgs(input1,'').callsFake(async function foo(){
                return "id/data not given";
            })
    
            let output = await db_repositry.edit_todo(input1,'');
            expect(output).to.equal("id/data not given");
    
        });
  
    it('edit_todo() should edit the database if id is passed in', async ()=> {
        var input1 = {
                title: "TODO242",
                _id:"242",
                discription: "After need to do",
                todayDate: "12/1/17",
                targetDate: "12/11/20"
            }
            let db_repositry = new dbRepositry();
            
            var stub = sinon.stub(db_repositry,'edit_todo');
        
            stub.withArgs(input1,'242').callsFake(async function foo(){
                return "user edited succssfully";
            })
    
            let output = await db_repositry.edit_todo(input1,'242');
            expect(output).to.equal("user edited succssfully");
    
        });
    
  });  

describe('todo show in database', function() {

    it('show_todo() should show data if database reading is proper', async function() {
        let db_repositry = new dbRepositry();
        
        var stub = sinon.stub(db_repositry,'show_todo');
       
        stub.withArgs().callsFake(async function foo(){
            return "data shown properly";
        })

        let output = await db_repositry.show_todo();
        expect(output).to.equal("data shown properly");
    });  
});