import * as fs from 'fs';

export class fileOperation
{
    file_data:string;

    readFile(file_name:string):string{
        this.file_data = fs.readFileSync(file_name, 'utf8')
        return this.file_data;
    }
     writeFile(file_name:string, parse_data:any):void{
        var data = JSON.stringify(parse_data, null, 1);
        fs.writeFileSync(file_name, data); 
     }
}
    