import {fileOperation} from './fileOperation'

let file_operation = new fileOperation();

var file_data_1 = file_operation.readFile('./array1.json')
var parse_data_1 = JSON.parse(file_data_1);

var file_data_2 = file_operation.readFile('./array2.json')
var parse_data_2 = JSON.parse(file_data_2);

for(var i=0; i<parse_data_1.Data1.length; i++)
{
    var id = parse_data_1.Data1[i].modelID;
    var index = parse_data_2.Data2.findIndex(function(item:any, i:any){
        return item.ModelId == id;
    });

    parse_data_1.Data1[i].MonthlyTargetVolume = parse_data_2.Data2[index].MonthlyTargetVolume;
}
    
file_operation.writeFile('./array1.json',parse_data_1);   
    