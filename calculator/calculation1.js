var input = [];
var inputStr = [];

function calculate(val)
{
    //for getting first value
    if(!input[1] && (IsSymbole(val)==1))         
    {
            //error controle for decimal
            if(val=='.' && (IsDecimal(input[0])==1));  
            else
            {
                if(input[0])
                {
                  val = input[0]+val;
                  var ele = inputStr.pop();
                }
                 input[0] = val;
                 inputStr.push(input[0]);
                 document.getElementById("val1").value = inputStr.join(''); 
            }
 
    }

      //to take the operations to be done
   else if(!input[2] && (IsSymbole(val)==0))            
   {
          if(val == '=')  
           equalto();
          if(IsTrigo(val)==1)                     
          {
            if(input[1])
            {  
              var ele = inputStr.pop();
            }
            var val2 = CalculateTrigo(val);
            document.getElementById("val1").value = inputStr.join('');
            document.getElementById("val").value = val2; 
            input = [];
            input[0] = val2;  
          }
          else                                 
          {
            if(input[1])
            {
            var ele = inputStr.pop();
            }
            input[1] = val;
            inputStr.push(val);   
            document.getElementById("val1").value = inputStr.join(''); 
          }
   }

   //to take the second digit                   
   else
   {
        if(val=='.' && (IsDecimal(input[2])==1));
        else if(IsSymbole(val)==1)
        {
            if(input[2])
             {
                val = input[2]+val;
                var ele = inputStr.pop();
             }
            input[2] = val;
            inputStr.push(input[2]);   
            document.getElementById("val1").value = inputStr.join('');
        }  
        else
        {
            var op = arithmatic(input[0], input[2], input[1]);                 
            input = [];
            input[0] = op;
     
            if(val == '=')   equalto(); 
            else if(IsTrigo(val)==1)             
             {
                 var val2;
                 val2 = CalculateTrigo(val);
                 document.getElementById("val1").value = inputStr.join('');
                 document.getElementById("val").value = val2; 
                 input = [];
                 input[0] = val2;  
             }
            else
            {
                input[1] = val;
                inputStr.push(input[1]);                                         
                document.getElementById("val1").value = inputStr.join('');
                document.getElementById("val").value = input[0];  
            }
        }  
   }

}

function resett()                 
{
    document.getElementById("val").value = "";
    document.getElementById("val1").value = "";
    input = [];
    inputStr = [];
} 

function equalto()
{
    inputStr = [];
    inputStr.push(input[0]); 
    document.getElementById("val1").value = inputStr.join('');
    document.getElementById("val").value = input[0];
}

function IsSymbole(val)        
{
    if(val != '=' && val!= '/' && val!= 'x' && val!= '-' && val!= '+' && val!= "Sin" && val!= "Cos" && val!= "log")
    {
        return(1);
    }
    else
    {
        return(0);
    }
}

function IsDecimal(dig)      
{
    var newDig = dig.split('');   
    for(var i=0; i < newDig.length; i++)
    {
            if(newDig[i] == '.')
            {
                return(1);
            }
    }

    return(0);

}

function IsTrigo(val)       
{
    if(val == "Sin" || val == "Cos" || val == "log")
    {
        return(1);
    }
}

function CalculateTrigo(val)       
{
    var val2;
    var val1 = input[0];
    switch(val)
    {
        case "Sin":
                  val2 = Math.sin(val1);
                  var str = "sin("+ inputStr.join('') + ")";
                    inputStr = [];
                    inputStr.push(str);
                  break;
        case "Cos":
                  val2 = Math.cos(val1);
                  var str = "cos("+ inputStr.join('') + ")";
                    inputStr = [];
                    inputStr.push(str);
                  break;
        case "log":
                  val2 = Math.log(val1);
                  var str = "log("+ inputStr.join('') + ")";
                    inputStr = [];
                    inputStr.push(str);
    }
    return(val2);
}

function arithmatic(operand1, operand2, operator)         
{
    var val1;


     switch(operator)
    {
        case '/':
                 if (operand2 == 0) throw new Error("Division By zero error!");
                 else    
                  val1 = operand1/operand2;
                  break;
        case 'x':
                  val1 = operand1*operand2;   
                  break;
        case '-':
                  val1 = operand1-operand2;
                  break;
        case '+':
                  val1 = parseFloat(operand1) + parseFloat(operand2);        
    }
    return(val1);
}